require "rails_helper"

RSpec.describe Games::Index do
  let(:games) { create_list(:game, 3) }

  subject { described_class.new(games) }

  describe 'as_json' do
    it 'returns proper representation' do
      expected_json = games.map do |game|
        {
          id: game.id,
          name: game.name,
          score: game.score,
          completed?: game.completed?,
          frames: game.frames.map { |frame| presenter_for(frame).as_json }
        }
      end

      expect(subject.as_json).to eq(expected_json)
    end
  end
end
