require "rails_helper"

RSpec.describe Games::Show do
  let(:game) { create(:game) }

  subject { described_class.new(game) }

  describe 'as_json' do
    it 'returns proper representation' do
      expected_json = {
        id: game.id,
        name: game.name,
        score: game.score,
        completed?: game.completed?,
        frames: game.frames.map { |frame| presenter_for(frame).as_json }
      }

      expect(subject.as_json).to eq(expected_json)
    end
  end
end
