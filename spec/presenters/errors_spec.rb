require 'rails_helper'

RSpec.describe Errors do
  subject { described_class.new(errors) }

  let(:errors) do
    {
      user_id: [ 'is invalid', 'has been already taken' ],
      password: [ 'is not strong enough' ],
      password_confirmation: 'is not the same as password'
    }
  end

  describe '#as_json' do
    it 'returns flatten and human readable representation of errors' do
      expect(subject.as_json).to eq({
        errors: [
          'User is invalid',
          'User has been already taken',
          'Password is not strong enough',
          'Password confirmation is not the same as password'
        ]
      })
    end
  end
end
