require "rails_helper"

RSpec.describe Frames::Show do
  let(:frame) { create(:frame, :completed, roll_1: 5, roll_2: 5, roll_3: 5) }

  subject { described_class.new(frame) }

  describe 'as_json' do
    it 'returns proper representation' do
      expected_json = {
        id: frame.id,
        status: frame.status,
        score: frame.score,
        roll_1: frame.roll_1,
        roll_2: frame.roll_2,
        roll_3: frame.roll_3
      }

      expect(subject.as_json).to eq(expected_json)
    end
  end
end
