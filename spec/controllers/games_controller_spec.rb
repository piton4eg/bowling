require 'rails_helper'

RSpec.describe GamesController, type: :controller do
  subject { JSON(response.body).with_indifferent_access }

  describe 'POST #create' do
    let(:game_params) {{ name: 'The Game' }}
    let(:game) { create(:game) }

    before do
      expect(Games::Create).to receive(:new)
        .with(game_params)
        .and_return(game_service)
    end

    context 'when service succeed' do
      let(:game_service) do
        double(execute!: true, success?: true, game: game)
      end

      it 'returns 200' do
        post :create, params: game_params
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it 'returns Games::Show presenter' do
        expect(Games::Show).to receive(:new)
          .with(game)
          .and_return(double(:as_json))
        post :create, params: game_params
      end
    end

    context 'when service failed' do
      let(:game_service) do
        double(execute!: true, success?: false, errors: { name: 'invalid' })
      end

      it 'returns 422' do
        post :create, params: game_params
        expect(response).not_to be_success
        expect(response).to have_http_status(422)
      end

      it 'returns errors' do
        post :create, params: game_params
        expect(subject['errors']).to include 'Name invalid'
      end
    end
  end

  describe 'GET #show' do
    let(:params) {{ id: game.id }}
    let(:game) { create(:game) }

    context 'when game exists' do
      it 'returns 200' do
        get :show, params: params
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it 'returns Games::Show presenter' do
        expect(Games::Show).to receive(:new)
          .with(game)
          .and_return(double(:as_json))
        get :show, params: params
      end
    end

    context 'when game does not exist' do
      let(:params) {{ id: '123123' }}

      it 'returns 404' do
        expect {
          get :show, params: params
        }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'PUT #ball' do
    let(:game_params) {{ id: game.id.to_s, pins: '10' }}
    let(:game) { create(:game) }

    before do
      expect(Games::Ball).to receive(:new)
        .with(game_params)
        .and_return(game_service)
    end

    context 'when service succeed' do
      let(:game_service) do
        double(execute!: true, success?: true, game: game)
      end

      it 'returns 200' do
        put :ball, params: game_params
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it 'returns Games::Show presenter' do
        expect(Games::Show).to receive(:new)
          .with(game)
          .and_return(double(:as_json))
        put :ball, params: game_params
      end
    end

    context 'when service failed' do
      let(:game_service) do
        double(execute!: true, success?: false, errors: { name: 'invalid' })
      end

      it 'returns 422' do
        put :ball, params: game_params
        expect(response).not_to be_success
        expect(response).to have_http_status(422)
      end

      it 'returns errors' do
        put :ball, params: game_params
        expect(subject['errors']).to include 'Name invalid'
      end
    end
  end
end
