FactoryGirl.define do
  factory :frame do
    game

    Frame::STATUSES.keys.each do |key|
      trait key do
        status key
      end
    end
  end
end
