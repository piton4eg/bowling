require 'rails_helper'

RSpec.describe Games::Create do
  subject { described_class.new(**args) }

  let(:args) do
    { name: name }
  end

  let(:name) { 'The Game' }

  describe 'execution' do
    it 'creates new game' do
      expect { subject.execute! }.to change { Game.count }.by(1)
    end

    it 'creates game and one frame with correct attributes' do
      subject.execute!

      expect(subject.game.name).to eq(name)
      expect(subject.game.frames.count).to eq(1)
      expect(subject.game.frames.first.status).to eq('roll_1')
    end

    context 'when name is empty' do
      let(:name) { nil }

      it 'fails with name error' do
        subject.execute!
        expect(subject.errors[:name]).to include 'must be filled'
      end
    end
  end
end
