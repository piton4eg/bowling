require 'rails_helper'

RSpec.describe Games::Ball do
  subject { described_class.new(**args) }

  let(:args) do
    {
      id: id,
      pins: pins
    }
  end

  let(:id) { game.id.to_s }
  let(:pins) { 5 }
  let(:game) { create(:game) }

  describe 'validations' do
    context 'when id is empty' do
      let(:id) { nil }

      it 'fails with id error' do
        subject.execute!
        expect(subject.errors[:id]).to include 'must be filled'
      end
    end

    context 'when pins is empty' do
      let(:pins) { nil }

      it 'fails with pins error' do
        subject.execute!
        expect(subject.errors[:pins]).to include 'must be filled'
      end
    end

    context 'when pins is not integer' do
      let(:pins) { 'foo' }

      it 'fails with pins error' do
        subject.execute!
        expect(subject.errors[:pins]).to include 'must be an integer'
      end
    end

    context 'when pins more then 10' do
      let(:pins) { 11 }

      it 'fails with pins error' do
        subject.execute!
        expect(subject.errors[:pins]).to include 'must be less than or equal to 10'
      end
    end
  end

  describe 'execution' do
    context 'when there are no frames' do
      it 'fails with frame error' do
        subject.execute!
        expect(subject.errors[:frame]).to include 'is invalid'
      end
    end

    context 'when game is completed' do
      before do
        create_list(:frame, 10, :completed, game: game)
      end

      it 'fails with frame error' do
        subject.execute!
        expect(subject.errors[:frame]).to include 'is invalid'
      end
    end

    context 'when frame service is invalid' do
      let!(:frame) { create(:frame, :roll_1, game: game) }

      it 'fails with frame error' do
        expect(Frames::Roll1).to receive(:new)
          .and_return(double(invalid?: true))
        subject.execute!
        expect(subject.errors[:frame]).to include 'is invalid'
      end
    end
  end

  describe 'execution' do
    Frame::ACTIVE_STATUSES.each do |status|
      let!(:frame) { create(:frame, status, game: game) }

      it 'delegates execution to frame service' do
        expect("Frames::#{frame.status.classify}".constantize).to receive(:new)
          .with(game: game, frame: frame, pins: pins)
          .and_return(double(invalid?: true))
        subject.execute!
      end
    end
  end

  describe 'execution results' do
    let!(:frame) { create(:frame, :roll_1, game: game) }

    context 'when all strikes' do
      let(:pins) { 10 }

      before do
        30.times { described_class.new(**args).execute! }
      end

      it 'returns maximum score' do
        expect(game.reload.score).to eq 300
        expect(game.completed?).to be true
      end

      it 'does not allow to make more balls' do
        subject.execute!
        expect(subject.errors?).to be true
      end
    end

    context 'when all spares' do
      before do
        21.times { described_class.new(**args).execute! }
      end

      it 'returns average score' do
        expect(game.reload.score).to eq 150
        expect(game.completed?).to be true
      end

      it 'does not allow to make more balls' do
        subject.execute!
        expect(subject.errors?).to be true
      end
    end

    context 'when all zeros' do
      let(:pins) { 0 }

      before do
        20.times { described_class.new(**args).execute! }
      end

      it 'returns 0 score' do
        expect(game.reload.score).to eq 0
        expect(game.completed?).to be true
      end

      it 'does not allow to make more balls' do
        subject.execute!
        expect(subject.errors?).to be true
      end
    end
  end
end
