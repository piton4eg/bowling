require 'rails_helper'

RSpec.describe Frames::Roll2 do
  subject { described_class.new(**args) }

  let(:args) do
    {
      frame: frame,
      game: game,
      pins: pins
    }
  end

  let(:pins) { 5 }
  let(:game) { create(:game) }

  describe '#invalid?' do
    let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 5) }

    its(:invalid?) { is_expected.to be false }

    context 'when pins are wrong' do
      let(:pins) { 6 }

      its(:invalid?) { is_expected.to be true }
    end
  end

  describe '#execute' do
    context 'when common ball' do
      let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 0) }

      it 'sets roll_2 and update frame status to completed and create new frame' do
        expect { subject.execute! }.to change { game.frames.count }.by(1)
        expect(frame.reload.roll_2).to eq 5
        expect(frame.completed?).to be true
        expect(game.frames.last.roll_1?).to be true
      end
    end

    context 'when common ball and tenth frame' do
      let!(:frames) { create_list(:frame, 9, :completed, game: game) }
      let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 0) }

      it 'completes frame and does not create new frame' do
        expect { subject.execute! }.not_to change { game.frames.count }
        expect(frame.reload.completed?).to be true
      end
    end

    context 'when there was strike in previous frame' do
      let!(:previous_frame) { create(:frame, :strike, game: game, roll_1: 10, roll_2: 5) }
      let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 5) }

      it 'adds pins to previous frame and update previous frame status to completed' do
        subject.execute!
        expect(previous_frame.reload.roll_3).to eq 5
        expect(previous_frame.completed?).to be true
      end
    end

    context 'when spare and tenth frame' do
      let!(:frames) { create_list(:frame, 9, :completed, game: game) }
      let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 5) }

      it 'updates frame status to extra_2 and does not create new frame' do
        expect { subject.execute! }.not_to change { game.frames.count }
        expect(frame.reload.extra_2?).to be true
      end
    end

    context 'when spare and not tenth frame' do
      let!(:frame) { create(:frame, :roll_2, game: game, roll_1: 5) }

      it 'updates frame status to spare and creates new frame' do
        expect { subject.execute! }.to change { game.frames.count }.by(1)
        expect(frame.reload.spare?).to be true
      end
    end
  end
end
