require 'rails_helper'

RSpec.describe Frames::Extra1 do
  subject { described_class.new(**args) }

  let(:args) do
    {
      frame: frame,
      game: game,
      pins: pins
    }
  end

  let(:pins) { 5 }
  let(:game) { create(:game) }

  describe '#invalid?' do
    let!(:frame) { create(:frame, :extra_1, game: game, roll_1: 10) }

    its(:invalid?) { is_expected.to be false }
  end

  describe '#execute' do
    context 'when common ball' do
      # don't create 9 completed frames just for performance
      let!(:frame) { create(:frame, :extra_1, game: game, roll_1: 10) }

      it 'updates frame status to extra_2' do
        expect { subject.execute! }.not_to change { game.frames.count }
        expect(frame.reload.roll_2).to eq 5
        expect(frame.extra_2?).to be true
      end
    end

    context 'when there was strike in previous frame' do
      let!(:previous_frame) { create(:frame, :strike, game: game, roll_1: 10, roll_2: 5) }
      let!(:frame) { create(:frame, :extra_1, game: game, roll_1: 10, roll_2: 5) }

      it 'adds pins to previous frame and update previous frame status to completed' do
        subject.execute!
        expect(previous_frame.reload.roll_3).to eq 5
        expect(previous_frame.completed?).to be true
      end
    end
  end
end
