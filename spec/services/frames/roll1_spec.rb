require 'rails_helper'

RSpec.describe Frames::Roll1 do
  subject { described_class.new(**args) }

  let(:args) do
    {
      frame: frame,
      game: game,
      pins: pins
    }
  end

  let(:pins) { 5 }
  let(:game) { create(:game) }

  describe '#invalid?' do
    let!(:frame) { create(:frame, :roll_1, game: game) }

    its(:invalid?) { is_expected.to be false }
  end

  describe '#execute' do
    context 'when common ball' do
      let!(:frame) { create(:frame, :roll_1, game: game) }

      it 'sets roll_1 and update frame status to roll_2' do
        subject.execute!
        expect(frame.reload.roll_1).to eq 5
        expect(frame.roll_2?).to be true
      end
    end

    context 'when there were strikes in previous frames' do
      let!(:before_previous_frame) { create(:frame, :strike, game: game, roll_1: 10, roll_2: 10) }
      let!(:previous_frame) { create(:frame, :strike, game: game, roll_1: 10) }
      let!(:frame) { create(:frame, :roll_1, game: game) }

      it 'adds pins to previous frames' do
        subject.execute!
        expect(previous_frame.reload.roll_2).to eq 5
        expect(before_previous_frame.reload.roll_3).to eq 5
        expect(before_previous_frame.completed?).to be true
      end
    end

    context 'when there was spare in previous frame' do
      let!(:previous_frame) { create(:frame, :spare, game: game, roll_1: 5, roll_2: 5) }
      let!(:frame) { create(:frame, :roll_1, game: game) }

      it 'adds pins to previous frame' do
        subject.execute!
        expect(previous_frame.reload.roll_3).to eq 5
      end
    end

    context 'when 10 pins and not tenth frame' do
      let!(:frame) { create(:frame, :roll_1, game: game) }
      let(:pins) { 10 }

      it 'update frame status to strike and creates new frame' do
        expect { subject.execute! }.to change { game.frames.count }.by(1)
        expect(frame.reload.roll_1).to eq 10
        expect(frame.strike?).to be true
        expect(game.frames.last.roll_1?).to be true
      end
    end

    context 'when 10 pins and tenth frame' do
      let!(:frames) { create_list(:frame, 9, :completed, game: game) }
      let!(:frame) { create(:frame, :roll_1, game: game) }
      let(:pins) { 10 }

      it 'update frame status to extra_1 and does not create new frame' do
        expect { subject.execute! }.not_to change { game.frames.count }
        expect(frame.reload.roll_1).to eq 10
        expect(frame.extra_1?).to be true
      end
    end
  end
end
