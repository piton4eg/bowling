require 'rails_helper'

RSpec.describe Frames::Extra2 do
  subject { described_class.new(**args) }

  let(:args) do
    {
      frame: frame,
      game: game,
      pins: pins
    }
  end

  let(:pins) { 5 }
  let(:game) { create(:game) }

  describe '#invalid?' do
    let!(:frame) { create(:frame, :extra_2, game: game, roll_1: 10, roll_2: 5) }

    its(:invalid?) { is_expected.to be false }

    context 'when pins are wrong' do
      let(:pins) { 6 }

      its(:invalid?) { is_expected.to be true }
    end
  end

  describe '#execute' do
    # don't create 9 completed frames just for performance
    let!(:frame) { create(:frame, :extra_2, game: game, roll_1: 10, roll_2: 5) }

    it 'sets pins and completes frame' do
      expect { subject.execute! }.not_to change { game.frames.count }
      expect(frame.reload.roll_3).to eq 5
      expect(frame.completed?).to be true
    end
  end
end
