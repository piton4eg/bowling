require 'rails_helper'

RSpec.describe Game, type: :model do
  let(:game) { create(:game) }

  describe '#score' do
    subject { game }

    context 'maximum score' do
      before do
        create_list(:frame, 10, :completed, game: game, roll_1: 10, roll_2: 10, roll_3: 10)
      end

      its(:score) { is_expected.to eq 300 }
    end

    context 'minimum score' do
      before do
        create_list(:frame, 10, :completed, game: game, roll_1: 0, roll_2: 0, roll_3: 0)
      end

      its(:score) { is_expected.to eq 0 }
    end

    context 'when game is in progress' do
      before do
        create_list(:frame, 5, :completed, game: game, roll_1: 5, roll_2: 5, roll_3: 5)
        create(:frame, :roll_2, game: game, roll_1: 5)
      end

      its(:score) { is_expected.to eq 75 }
    end
  end

  describe '#active_frame' do
    subject { game }

    context 'when there is active frame' do
      let!(:completed_frame) { create(:frame, :completed, game: game) }
      let!(:active_frame) { create(:frame, :roll_1, game: game) }

      its(:active_frame) { is_expected.to eq active_frame }
    end

    context 'when there are no active frames' do
      let!(:completed_frame) { create(:frame, :completed, game: game) }
      let!(:another_completed_frame) { create(:frame, :completed, game: game) }

      its(:active_frame) { is_expected.to be_nil }
    end
  end

  describe '#completed?' do
    subject { game }

    context 'when less than 10 frames completed' do
      let!(:frame) { create(:frame, :completed, game: game) }

      its(:completed?) { is_expected.to be false }
    end

    context 'when 10 frames completed' do
      let!(:frames) { create_list(:frame, 10, :completed, game: game) }

      its(:completed?) { is_expected.to be true }
    end

    context 'when 10 frames but not all completed' do
      let!(:frames) { create_list(:frame, 9, :completed, game: game) }
      let!(:frame) { create(:frame, :roll_1, game: game) }

      its(:completed?) { is_expected.to be false }
    end
  end
end
