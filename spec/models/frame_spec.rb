require 'rails_helper'

RSpec.describe Frame, type: :model do
  let(:game) { create(:game) }
  let(:frame) { build(:frame, game: game) }

  describe '#max_frames_in_game' do
    subject { frame }

    context 'when there were 9 frames' do
      before { create_list(:frame, 9, :completed, game: game) }

      it { is_expected.to be_valid }
    end

    context 'when there were 10 frames' do
      before { create_list(:frame, 10, :completed, game: game) }

      it { is_expected.not_to be_valid }
      it { expect(Frame.last).to be_valid }
    end
  end

  describe '#only_one_active_frame' do
    let(:frame) { build(:frame, :roll_1, game: game) }

    subject { frame }

    context 'when there is another active frame' do
      before { create(:frame, :roll_2, game: game) }

      it { is_expected.not_to be_valid }
      it { expect(Frame.last).to be_valid }
    end

    context 'when there is another inactive frame' do
      before { create(:frame, :completed, game: game) }

      it { is_expected.to be_valid }
    end
  end

  describe '#score' do
    subject { frame }

    context 'when frame is not completed' do
      let(:frame) { create(:frame, :roll_2, roll_1: 5) }

      its(:score) { is_expected.to be_nil }
    end

    context 'when frame is completed' do
      let(:frame) { create(:frame, :completed, roll_1: 5, roll_2: 5, roll_3: 10) }

      its(:score) { is_expected.to eq 20 }
    end
  end

  describe '#tenth_frame?' do
    subject { frame }

    context 'when there are 10 frames' do
      before { create_list(:frame, 10, :completed, game: game) }

      its(:tenth_frame?) { is_expected.to be true }
    end

    context 'when there are less then 10 frames' do
      before { create_list(:frame, 9, :completed, game: game) }

      its(:tenth_frame?) { is_expected.to be false }
    end
  end

  describe '#previous' do
    context 'when there is previous frame' do
      subject { frame }

      let!(:previous_frame) { create(:frame, :completed, game: game) }
      let!(:frame) { create(:frame, :completed, game: game) }

      its(:previous) { is_expected.to eq previous_frame }
    end

    context 'when there are no previous frames' do
      subject { frame }

      let!(:frame) { create(:frame, :completed, game: game) }

      its(:previous) { is_expected.to be_nil }
    end
  end
end
