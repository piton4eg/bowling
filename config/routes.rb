Rails.application.routes.draw do
  scope '/api' do
    resources :games, only: [:create, :show, :index] do
      put :ball, on: :member
    end
  end
end
