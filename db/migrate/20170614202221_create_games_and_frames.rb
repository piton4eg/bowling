class CreateGamesAndFrames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name, null: false

      t.timestamps
    end

    create_table :frames do |t|
      t.integer :status, default: 10, null: false
      t.references :game, index: true

      t.integer :roll_1
      t.integer :roll_2
      t.integer :roll_3

      t.timestamps
    end
  end
end
