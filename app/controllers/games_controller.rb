class GamesController < ApplicationController
  def create
    service = Games::Create.new(name: params[:name])
    service.execute!

    if service.success?
      render json: Games::Show.new(service.game).as_json
    else
      render json: Errors.new(service.errors).as_json, status: 422
    end
  end

  def show
    render json: Games::Show.new(game).as_json
  end

  def index
    render json: Games::Index.new(games).as_json
  end

  def ball
    service = Games::Ball.new(id: params[:id], pins: params[:pins])
    service.execute!

    if service.success?
      render json: Games::Show.new(service.game).as_json
    else
      render json: Errors.new(service.errors).as_json, status: 422
    end
  end

  private def game
    @game ||= Game.find(params[:id])
  end

  private def games
    @games ||= Game.all
  end
end
