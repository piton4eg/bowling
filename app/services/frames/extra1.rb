module Frames
  class Extra1 < Frames::Base
    def execute!
      frame.roll_2 = pins

      if previous_frame&.strike?
        previous_frame.roll_3 = pins
        previous_frame.completed!
      end

      frame.extra_2!
    end
  end
end
