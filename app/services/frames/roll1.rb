module Frames
  class Roll1 < Frames::Base
    def execute!
      frame.roll_1 = pins

      if previous_frame&.strike?
        previous_frame.update(roll_2: pins)
        if before_previous_frame&.strike?
          before_previous_frame.roll_3 = pins
          before_previous_frame.completed!
        end
      elsif previous_frame&.spare?
        previous_frame.roll_3 = pins
        previous_frame.completed!
      end

      if pins == 10
        if frame.tenth_frame?
          frame.extra_1!
        else
          frame.strike!
          game.frames.create(status: :roll_1)
        end
      else
        frame.roll_2!
      end
    end
  end
end
