module Frames
  class Roll2 < Frames::Base
    def invalid?
      pins > (10 - frame.roll_1)
    end

    def execute!
      frame.roll_2 = pins

      if previous_frame&.strike?
        previous_frame.roll_3 = pins
        previous_frame.completed!
      end

      if (frame.roll_1 + frame.roll_2) == 10
        if frame.tenth_frame?
          frame.extra_2!
        else
          frame.spare!
          game.frames.create(status: :roll_1)
        end
      else
        frame.completed!
        frame.tenth_frame? ? frame : game.frames.create(status: :roll_1)
      end
    end
  end
end
