module Frames
  class Extra2 < Frames::Base
    def invalid?
      (frame.roll_1 == 10) && (frame.roll_2 < 10) && (pins > 10 - frame.roll_2)
    end

    def execute!
      frame.roll_3 = pins
      frame.completed!
    end
  end
end
