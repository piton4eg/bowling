module Frames
  class Base < ApplicationService
    def invalid?
      false
    end

    protected def previous_frame
      @previous_frame ||= frame.previous
    end

    protected def before_previous_frame
      @before_previous_frame ||= previous_frame&.previous
    end
  end
end
