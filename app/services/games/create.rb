module Games
  class Create < ApplicationService
    attr_reader :game

    schema do
      required(:name).filled(:str?)
    end

    def execute!
      super do
        @game = Game.create(name: name)
        @game.frames.create
      end
    end
  end
end
