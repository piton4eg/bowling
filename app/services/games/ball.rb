module Games
  class Ball < ApplicationService
    attr_reader :game

    schema do
      required(:id).filled(:str?)
      required(:pins).filled(:int?, gteq?: 0, lteq?: 10)
    end

    def execute!
      @game = Game.find(id)

      if frame.nil? || frame_service.invalid?
        fail!(errors: { frame: 'is invalid' })
        return
      end

      super { frame_service.execute! }
    end

    private def frame_service
      @frame_service ||= "Frames::#{frame.status.classify}".constantize.new(
        game: game,
        frame: frame,
        pins: pins
      )
    end

    private def frame
      @frame ||= game.active_frame
    end
  end
end
