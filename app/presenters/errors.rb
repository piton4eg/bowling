class Errors < ApplicationPresenter
  def initialize(errors)
    @errors = errors
  end

  def as_json
    result = @errors.map do |key, messages|
      attr_name = key.to_s.humanize
      Array.wrap(messages).map { |message| "#{attr_name} #{message}" }
    end

    { errors: result.flatten }
  end
end
