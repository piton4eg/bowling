require 'presenter_for'

class ApplicationPresenter
  include PresenterFor

  protected def convert_to_json(record, params = {})
    json = record.as_json(params)
    yield json if block_given?
    json.deep_symbolize_keys.delete_if { |_, v| v.nil? }
  end
end
