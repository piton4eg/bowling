module Frames
  class Show < ApplicationPresenter
    ATTRIBUTES = %i[id status roll_1 roll_2 roll_3].freeze
    METHODS = %i[score].freeze

    attr_reader :frame

    def initialize(frame)
      @frame = frame
    end

    def as_json
      convert_to_json(frame, only: ATTRIBUTES, methods: METHODS)
    end
  end
end
