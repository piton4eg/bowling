module Games
  class Show < ApplicationPresenter
    ATTRIBUTES = %i[id name].freeze
    METHODS = %i[score completed?].freeze

    attr_reader :game

    def initialize(game)
      @game = game
    end

    def as_json
      convert_to_json(game, only: ATTRIBUTES, methods: METHODS) do |json|
        json['frames'] = game.frames.map { |frame| presenter_for(frame).as_json }
      end
    end
  end
end
