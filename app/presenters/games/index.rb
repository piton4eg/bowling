module Games
  class Index < ApplicationPresenter
    attr_reader :games

    def initialize(games)
      @games = games
    end

    def as_json
      games.map { |game| presenter_for(game).as_json }
    end
  end
end
