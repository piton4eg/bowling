class Frame < ApplicationRecord
  belongs_to :game

  STATUSES = {
    roll_1: 10,
    roll_2: 20,
    strike: 30,
    spare: 40,
    completed: 50,
    extra_1: 60,
    extra_2: 70
  }.freeze
  ACTIVE_STATUSES = %i(roll_1 roll_2 extra_1 extra_2)

  enum status: STATUSES

  validate :max_frames_in_game, on: :create
  validate :only_one_active_frame, on: :create

  def score
    return unless completed?
    [roll_1, roll_2, roll_3].compact.sum
  end

  def tenth_frame?
    game.frames.count == 10
  end

  def previous
    game.frames.where("id < ?", id).last
  end

  private def max_frames_in_game
    errors.add(:base, 'too much frames in the game') if game.frames.count >= 10
  end

  private def only_one_active_frame
    if game.frames.where(status: ACTIVE_STATUSES).count >= 1
      errors.add(:status, 'there is active frame in the game already')
    end
  end
end
