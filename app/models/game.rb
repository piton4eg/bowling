class Game < ApplicationRecord
  has_many :frames

  def score
    frames.map(&:score).compact.sum
  end

  def active_frame
    frames.find_by(status: Frame::ACTIVE_STATUSES)
  end

  def completed?
    frames.all?(&:completed?) && frames.count == 10
  end
end
