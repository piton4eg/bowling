# Bowling

## Setup:

- install and run postgresql
- bundle exec rake db:create db:migrate
- rails s
- make corresponding requests to http://localhost:3000/api

## API examples:

- `curl -X GET http://localhost:3000/api/games` - gets info for all games
- `curl -X POST http://localhost:3000/api/games/ -d name="{NAME}"` - creates new game with name NAME
- `curl -X PUT http://localhost:3000/api/games/{ID}/ball -d pins={PINS}` - sets PINS number of pins knocked down by ball for game with ID
- `curl -X GET http://localhost:3000/api/games/{ID}` - gets info for game with ID
